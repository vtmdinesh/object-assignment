const getKeysFunction = (testObject) => {
  
    const keysArray = []

    if (!(typeof(testObject) === "object")) {
        return []
    }
    else {
        for (let item in testObject) {
            keysArray.push(item)
        }
        return keysArray
    }
}

module.exports = getKeysFunction 