const getValuesFunction = (testObject={}) => {

    let valuesArray = []
    let value

   

    if (!(typeof(testObject) === "object")) {
        return []
    }
    else{       
        for(let item in testObject){
            value = testObject[item]
            valuesArray.push(value)
        }
    }
    return valuesArray
}

module.exports = getValuesFunction 