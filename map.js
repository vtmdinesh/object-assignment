const mapObjectFunction = (testObject = {}, testFunction = (key, value) => { return value }) => {

    let value
    let updatedValue

    if (!(testObject.constructor === Object)) {
        return {}
    }
    else {
        for (let item in testObject) {
            value = testObject[item]
            updatedValue = testFunction(item, value)
            testObject[item] = updatedValue
        }
    }
    return testObject
}

module.exports = mapObjectFunction