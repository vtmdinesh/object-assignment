const getDefaultsFunction = (testObject = {}, defaultProps = {}) => {

    let invertedObject = {}
    let value
    let pair

    if (!(testObject.constructor === Object && defaultProps.constructor === Object)) {
        return {}
    }
    else {
        for (let item in defaultProps) {

            if (testObject[item] === undefined) {
                testObject[item] = defaultProps[item]
            }
        }
    }
    return testObject
}

module.exports = getDefaultsFunction 