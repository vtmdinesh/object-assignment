const getPairsFunction = (testObject={}) => {

    let pairsArray = []
    let value
    let pair
        
    if (!(testObject.constructor === Object)){
        return []
    }
    else {

        for(let item in testObject){
            value = testObject[item]
            pair = [item,value]
            pairsArray.push(pair)
        }
    }
    return pairsArray
}

module.exports = getPairsFunction 