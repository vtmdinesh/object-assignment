const getInvertFunction = (testObject) => {

    let invertedObject = {}
    let value
    let pair
    if (!(testObject.constructor === Object)) {
        return {}
    }
    else {
        for (let item in testObject) {
            value = testObject[item]
            invertedObject[value] = item

        }
    }
    return invertedObject
}

module.exports = getInvertFunction 